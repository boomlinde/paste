import time
import hashlib
import base64
import flask
import sqlitedict

app = flask.Flask('paste')

DB_SETTINGS = {
    'filename': 'paste.db',
    'autocommit': True
}

def save_paste(text):
    db = sqlitedict.SqliteDict(**DB_SETTINGS)
    hash = base64.urlsafe_b64encode(
        hashlib.md5(text).digest()[:8])[:-1]
    db[hash] = text
    db.close()
    return hash

def load_paste(hash):
    db = sqlitedict.SqliteDict(**DB_SETTINGS)
    paste = db[hash]
    db.close()
    return paste

@app.route('/')
def root():
    return flask.redirect('/paste')

@app.route('/paste', methods=['GET', 'POST'])
def main():
    if (flask.request.method == 'POST'
    and 'text' in flask.request.form):
        return flask.redirect('/paste/%s' %
            save_paste(flask.request.form['text']))
    else:
        return '''
        <!DOCTYPE html>
        <html><head><title>Paste</title></head>
        <body>
        <form action="paste" method="POST">
        <div>
        <textarea name="text" rows="25" cols="80"></textarea>
        </div>
        <div><input type="submit" value="Upload"></div>
        </form>
        </body></html>
        '''

@app.route('/paste/<hash>')
def paste(hash):
    return flask.Response(
        load_paste(hash),
        mimetype='text/plain'
    )

if __name__ == '__main__':
    app.run(debug=True)
